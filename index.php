<html>
    <head>
        
        <meta charset="utf-8">
        <title>index.html</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="morad" />
        <!-- css -->
        <link href="css/bootstrap.css" rel="stylesheet" />
		<link href="css/style.css" rel="stylesheet" />
		
	
		
        
    </head>
    <body>
        


<nav class="navbar navbar-red">
<div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand" href="#">Belarbi <span> Morad </span></div>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
		<li><a href="#">Home</a></li>
		<li><a href="#">Contact</a></li>

      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

		
		
			<section class="module parallax parallax-1">
				<div class="container">
					
					<div class="col-md-4">
					<div class="post">
					<h1>BELARBI Morad</h1>
					<h3>Développeur web full stack</h3>
					<P>Développeur web full stack,  python, jQuery, PHP ainsi que les Frameworks associés.
					   Passionné par le web depuis le collège, je vous propose de découvrir comment intégrer un système
					   de multi-Tag facilement avec un plugin sur le quelle j’ai eu l’occasion de travailler.
					</P>
					<h3> Cette page vous a plu ?</h3>
					<p>Recommandez-la ! </p>

					<h3> Projet</h3>
					<ul>
						<li> EazTag.js</li>
					</ul>
					</div>
					</div>

					<div class="col-md-8">
						<div class="jumb">
						<center>
							<h1> EazTag.js </h1>
						<h2>  Intégrer facilement un système de tag a vos pages web.</h2>					
							<a href="" class="btn btn-red-orange">Télécharger</a>
							<a href="demo/demo.html" class="btn btn-red-orange">Démo</a>
							<a href="" class="btn btn-red-orange">Git</a>
						</center>
					</div>
					</div>
					
				</div>
			</section>
			
			<section class="module parallax parallax-2">
				<div class="container">
					<div class="col-md-10">
				<h2>Utilisation basic.</h2>
				
				<p> Tous d'abord je vous invite à Télécharger les sources,  soit directement sur le site avec une extension
				.zip ou sur git hub. Puis placé le répertoire dans votre projet.
				</p>
				
				<h2> Inclure le plugin.</h2>
				
				<p>Placer les lignes suivantes entre les balises head en respectant les liens de votre projet.</p>
				<?php
								include_once('geshi/geshi.php');


				$source ='<head>
<link href="css/eazTag.css" rel="stylesheet" />
<script src="js/jquery.js"></script>  <!-- Attention le plugin a besoins de jquery !-->
<script src="eazTag.js"></script> 
</head>';
				$language = 'javascript';
				
				$geshi = new GeSHi($source, $language);
				$geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
				echo $geshi->parse_code();
				
				?>
				
				<p> Puis créer un div avec la class="EazTag" et donné lui un id pour l'exemple ce serra "demo". </p>
				
				<?php
				$source2 ='<html>
<div class="EazTag" id="demo"> </div>
</html>';
				
				
				$geshi2 = new GeSHi($source2, $language);
				$geshi2->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
				echo $geshi2->parse_code();
				
				?>
				
				<h2> Configuration.</h2>
				
				<p> Et enfin faire appel au plugin en utilisant les paramètres. </p>
				<?php
				
$source3 ='<script type="text/javascript">
$("#demo").eazTag(
	{
		"prefilled":{
		"Espagne":"Espagne",
		"France":"France"
					}, 
                            
		"Choices":{
		"Suisse":"Suisse",
                  },
		render:function(data){
				// data est et une liste de tags selectionés
				console.log(data);
							 }
    });
</script>	
	';
	
	$geshi3 = new GeSHi($source3, $language);
				$geshi3->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
				echo $geshi3->parse_code();
				
				
				?>
	
	<p>Quelques explications, à la ligne 4 prefilled est la liste de tags qui sont automatiquement sélectionnés,
	alors que Choices à la ligne 9 est la liste des tags qui sont pas sélectionnés au préalable.
	A savoir que prefilled est facultative est que les tags présent dans prefilled est choices seront affiché uniquement dans prefilled.
	</p>
	
	<p>
		La fonction render return data qui est la liste des tags sélectionnées est permet de faire un traitement de la réponse.  
	</p>
	
	<h2>Utilisation avancé.</h2>
	
	<p> Le plugin permet aussi d'interagir avec une api comme pour  <a href="" >la démo </a> dont les sources sont disponible sur <a href=""> git hub</a>. <br>
	Pour toute questions envoyer moi un mail et je prendrez le temps de vous répondre en attendant la possibilité de poster directement sur le site.
	</p>
					</div>
				
				
				
				
				
				</div>
			
			</section>
			                 
		<footer>
			
		<P>Copyright 2016 BELARBI Morad.</P>	
			
		</footer>	

       <script src="js/jquery.js"></script>
       <script src="js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
     
    </body>
</html>